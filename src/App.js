import React, { useEffect, useState, Fragment } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import axios from "axios";

const validate = ["undefined", "null", undefined, ""];

const ulli = (item, idx) => {
  if (validate.includes(item.submenu)) {
    return (
      <li>
        <Link to={item.url}>{item.label}</Link>
      </li>
    );
  }
  return (
    <div>
      <li>
        {validate.includes(item.url) ? (
          item.label
        ) : (
          <Link to={item.url}>{item.label}</Link>
        )}
      </li>
      <ul>{item.submenu.map(ulli)}</ul>
    </div>
  );
};

const routerNot = (item, idx) => {
  if (!validate.includes(item.submenu)) {
    item.submenu.map(routerNot);
  }
  if (!validate.includes(item.component)) {
    console.log(item.component);
    return (
      <Route exact path={item.url}>
        {components[item.component]}
      </Route>
    );
  }
};

export default function BasicExample() {
  const [rotas, setRotas] = useState([]);

  useEffect(() => {
    axios({
      url: "http://localhost:3001/login",
      method: "POST",
      withCredentials: true,
      data: {
        login: "admin"
      }
    })
      .then(o => {
        axios({
          url: "http://localhost:3001/menus",
          method: "GET",
          withCredentials: true
        })
          .then(ok => {
            setRotas(ok.data);
          })
          .then(err => console.log(err));
      })
      .catch(er => console.log(er));
  }, []);

  return (
    <Router>
      <div>
        {rotas.map(ulli)}

        <hr />

        <Switch>{rotas.map(routerNot)}</Switch>
      </div>
    </Router>
  );
}

const Home = () => {
  console.log("home");
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
};

const Senha = () => {
  console.log("teste");
  return (
    <div>
      <h2>Senha</h2>
    </div>
  );
};

const Chamados = () => {
  return (
    <div>
      <h2>Chamados</h2>
    </div>
  );
};

const Config = () => {
  return (
    <div>
      <h2>Config</h2>
    </div>
  );
};

const components = {
  Home,
  Senha,
  Chamados,
  Config
};
